package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FactorialTest {

    @Test
    public void FactorialTest() {
        Factorial fact = new Factorial();
        int n = 5;
        long expectedResult = 120;

        long result = fact.factorial(n);
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void factorialRecursiveTest() {
        Factorial fact = new Factorial();
        int n = 6;
        long expectedResult = 720;

        long result = fact.factorialRecursive(n);
        Assertions.assertEquals(expectedResult, result);
    }
}
