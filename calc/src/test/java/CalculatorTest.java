import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Mukan Atazhanov
 * @project hw1
 * @created 18/02/2022 - 21:49
 */
class CalculatorTest {

    @Test
    void add() {
        int a = 10;
        int b = 10;
        int expected = 20;

        Calculator calc = new Calculator();
        int res = calc.add(a, b);
        assertEquals(expected, res);
    }

    @Test
    void subtract() {
        int a = 10;
        int b = 10;
        int expected = 0;

        Calculator calc = new Calculator();
        int res = calc.subtract(a, b);
        assertEquals(expected, res);
    }

    @Test
    void multiply() {
        int a = 10;
        int b = 10;
        int expected = 100;

        Calculator calc = new Calculator();
        int res = calc.multiply(a, b);
        assertEquals(expected, res);
    }

    @Test
    void divide() {
        int a = 10;
        int b = 10;
        int expected = 1;

        Calculator calc = new Calculator();
        int res = calc.divide(a, b);
        assertEquals(expected, res);
    }
}